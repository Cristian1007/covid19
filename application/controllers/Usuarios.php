<?php
      class Usuarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");

        }


                public function index(){
                  $data["listadoUsuarios"]=$this->usuario->consultarTodos();
                  $this->load->view("header");
                  $this->load->view("usuarios/index",$data);
                  $this->load->view("footer");
                }
                public function nuevo(){


                  $this->load->view("header");
                  $this->load->view("usuarios/nuevo");
                  $this->load->view("footer");
                }

        public function editar($id_usu){

          $data["usuario"]=$this->usuario->consultarPorId($id_usu);
          $this->load->view("header");
          $this->load->view("usuarios/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
            $id_usu=$this->input->post("id_usu");

            $datosUsuarioEditado=array(
              "apellido_usu"=>$this->input->post("apellido_usu"),
              "nombre_usu"=>$this->input->post("nombre_usu"),
              "perfil_usu"=>$this->input->post("perfil_usu"),
              "email_usu"=>$this->input->post("email_usu"),
              "password_usu"=>$this->input->post("password_usu"),
              "estado_usu"=>$this->input->post("estado_usu")
            );



            if($this->usuario->actualizar($datosUsuarioEditado,$id_usu)){
                //echo "INSERCION EXITOSA";



            }else{
                echo "ERROR AL ACTUALIZAR";
            }
            redirect("usuarios/index");
        }

        public function guardarusuario(){
            $datosNuevoUsuario=array(
                "apellido_usu"=>$this->input->post("apellido_usu"),
                "nombre_usu"=>$this->input->post("nombre_usu"),
                "perfil_usu"=>$this->input->post("perfil_usu"),
                "email_usu"=>$this->input->post("email_usu"),
                "estado_usu"=>$this->input->post("estado_usu"),
                "password_usu"=>$this->input->post("password_usu"),
            );


            if($this->usuario->insertar($datosNuevoUsuario)){

            }else{
               echo "error";
            }
            redirect("usuarios/index");
        }



        public function procesarEliminacion($id_usu){


            if($this->usuario->eliminar($id_usu)){
               $imagen=$this->usuario->consultarPorId($id_usu);

            }else{
                echo "ERROR AL ELIMINAR";
            }
            redirect("usuarios/index");
          }


    }//cierre de la clase
