<?php
      class Vacunas extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("vacuna");
            $this->load->model("usuario");

        }

        public function index(){
          $data["listadoVacunas"]=$this->vacuna->consultarTodos();
          $this->load->view("header");
          $this->load->view("vacunas/index",$data);
          $this->load->view("footer");
        }

        public function nuevo(){
          $this->load->view("header");
          $this->load->view("vacunas/nuevo");
          $this->load->view("footer");
        }

      public function insertarVacuna(){

        $datosNuevoVacuna=array(
          "nombre_vac"=>$this->input->post("nombre_vac"),
          "fabricante_vac"=>$this->input->post("fabricante_vac"),
          "cantidad_vac"=>$this->input->post("cantidad_vac"),
          "estado_vac"=>$this->input->post("estado_vac")
      );

        if($this->vacuna->insertarVac($datosNuevoVacuna)){

        }else{
          echo "error";
        }
        redirect("vacunas/index");
}
      public function eliminarVacuna(){
          $id_vac=$this->input->post("id_vac");
          if($this->vacuna->eliminar($id_vac)){

          }else{
            echo "Error";
          }
          redirect("vacunas/index");
      }

        public function procesarEliminacion($id_vac){
          if ($this->vacuna->eliminar($id_vac)) {
            redirect("vacunas/index");
          }else {
            echo "no eliminado";
          }

        }

        public function editar($id_vac){
          $data["vacuna"]=$this->vacuna->obtenerPorId($id_vac);
          $this->load->view("header");
          $this->load->view("vacunas/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_vac=$this->input->post("id_vac");
          $datosVacunaEditado=array(
            "nombre_vac"=>$this->input->post("nombre_vac"),
            "tipo_vac"=>$this->input->post("tipo_vac"),
            "fabricante_vac"=>$this->input->post("fabricante_vac"),
            "path_vac"=>$this->input->post("path_vac"),
            "estado_vac"=>$this->input->post("estado_vac")
        );
          if($this->vacuna->actualizar($id_vac,$datosVacunaEditado)){
              redirect("vacunas/index");
          }else{
            echo "error al editar";
          }
        }

        }
?>
