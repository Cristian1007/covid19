<?php
      class Generos extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model('genero');
        }

        public function index(){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $this->load->view("header");
          $this->load->view("generos/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("generos/nuevo");
          $this->load->view("footer");
        }

        public function guardarGenero(){
            $datosNuevoGenero=array(
                "nombre_gen"=>$this->input->post("nombre_gen")
            );
            if($this->genero->insertar($datosNuevoGenero)){

            }else{
              echo "Error";
            }
            redirect("generos/index");
        }

        public function procesarEliminacion($id_gen){
          if ($this->genero->eliminar($id_gen)) {

          }else {
            echo "Error";

          }
          redirect("generos/index");

        }

        public function editar($id_gen){
          $data["genero"]=$this->genero->consultarPorId($id_gen);
          $this->load->view("header");
          $this->load->view("generos/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_gen=$this->input->post("id_gen");
          $datosGeneroEditado=array(
            "nombre_gen"=>$this->input->post("nombre_gen")
        );
          if($this->genero->actualizar($id_gen,$datosGeneroEditado)){


          }else{
            echo "Error";
          }
          redirect("generos/index");
        }

        }
?>
