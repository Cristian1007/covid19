<?php
      class Dosiss extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("dosis");
            $this->load->model("vacuna");
            $this->load->model("persona");
            $this->load->model("vacunador");

        }

        public function index(){
          $data["listadoDosiss"]=$this->dosis->consultarTodos();
          $data ["listadoVacunas"]=$this->vacuna->consultarTodos();
          $data ["listadoPersonas"]=$this->persona->consultarTodos();
          $data ["listadoVacunador"]=$this->vacunador->consultarTodos();
          $this->load->view("header");
          $this->load->view("dosiss/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data ["listadoVacunas"]=$this->vacuna->consultarTodos();
            $data ["listadoPersonas"]=$this->persona->consultarTodos();
            $data ["listadoVacunador"]=$this->vacunador->consultarTodos();
          $this->load->view("header");
          $this->load->view("dosiss/nuevo",$data);
          $this->load->view("footer");
        }

        public function guardarDosis(){
            $datosNuevoDosis=array(
              "fk_id_vac"=>$this->input->post("fk_id_vac"),
              "fk_id_vacu"=>$this->input->post("fk_id_vacu"),
              "fk_id_per"=>$this->input->post("fk_id_per"),
                "fecha_dos"=>$this->input->post("fecha_dos"),
                "lugar_dos"=>$this->input->post("lugar_dos"),
                "numero_lote_dos"=>$this->input->post("numero_lote_dos")

            );
            if($this->dosis->insertar($datosNuevoDosis)){

            }else{
              echo "error";
            }
            redirect("dosiss/index");
        }

        public function procesarEliminacion($id_dos){
          if ($this->dosis->eliminar($id_dos)) {

          }else {
            echo "error";
          }
          redirect("dosiss/index");

        }

        public function editar($id_dos){
          $data["dosis"]=$this->dosis->consultarPorId($id_dos);
          $data ["listadoVacunas"]=$this->vacuna->consultarTodos();
            $data ["listadoPersonas"]=$this->persona->consultarTodos();
            $data ["listadoVacunador"]=$this->vacunador->consultarTodos();

          $this->load->view("header");
          $this->load->view("dosiss/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
          $id_dos=$this->input->post("id_dos");
          $datosDosisEditado=array(
            "fecha_dos"=>$this->input->post("fecha_dos"),
            "lugar_dos"=>$this->input->post("lugar_dos"),
            "numero_lote_dos"=>$this->input->post("numero_lote_dos")
        );
          if($this->dosis->actualizar($id_dos,$datosDosisEditado)){

          }else{
          echo "error";
          }
          redirect("dosiss/index");
        }

        }
