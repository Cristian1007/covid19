<br><br><br>
<center>
<h1>LISTADO DE VACUNADORES</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/vacunadores/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <?php if ($listadoVacunadores): ?>

      <table class="table table-bordered table-hover table-striped" >
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoVacunadores->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_vacu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_vacu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->apellido_vacu; ?>
              </td>
              <td class="text-center">
                <?php if ($filaTemporal->estado_vacu=="1"): ?>
                  <div class="text-center" >
                    <p>ACTIVO</p>
                  </div>
                <?php else: ?>
                  <div class="text-center">
                    <p>INACTIVO</p>
                  </div>
                <?php endif; ?>
              </td>

              <td class="text-center">
                <a href="<?php echo site_url(); ?>/vacunadores/editar/<?php echo $filaTemporal->id_vacu; ?>" class="btn btn-warning"><i class="fa fa-pen"></i> Editar</a>
                <a href="<?php echo site_url(); ?>/vacunadores/procesarEliminacion/<?php echo $filaTemporal->id_vacu;?>"
                 class="btn btn-danger">
                  <i class="fa fa-trash"></i> Eliminar
                </a>
              </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3>No de encontraron vacunadores</h3>
      </div>
    <?php endif; ?>
  </div>
  <div class="col-md-1">

  </div>
</div>
