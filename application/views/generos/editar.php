<div class="container">

<br><br>
<h1 class="text-center">EDITAR GENERO</h1>
<br><br>
<form class="" action="<?php echo site_url(); ?>/generos/procesarActualizacion" method="post">
  <div class="row">
    <div class="col-md-4">
      <input type="hidden" name="id_gen" id="id_gen" value="<?php echo $genero->id_gen; ?>">
      <label for="">NOMBRE DEL GENERO: </label><br>
    </div>
    <div class="col-md-7">
      <select class="form-control" name="nombre_gen" id="nombre_gen" required>
        <option value=""> SELECCIONE UNA OPCION </option>
        <option value="MASCULINO">MASCULINO</option>
        <option value="FEMENINO">FEMENINO</option>
        <option value="TRANSEXUALES">TRANSEXUALES</option>
        <option value="NO BINARIO">NO BINARIO</option>

      </select>
    </div>
  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-7">
    <br>
    <button type="submit" name="button" class="btn btn-primary">EDITAR</button>
    &nbsp;&nbsp;&nbsp;

    <a href="<?php echo site_url(); ?>/generos/index" class="btn btn-warning">CANCELAR</a>
  </div>
  <br>

</form>

</div>
<script type="text/javascript">

  $("#nombre_gen").val("<?php echo $genero->nombre_gen; ?>")
</script>
