<br><br><br>
<center>
<h1>LISTADO DE GENEROS</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/generos/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="container">

<div class="row">

  <div class="col-md-12">
    <?php if ($listadoGeneros): ?>

      <table class="table table-bordered table-hover table-striped" id="tbl_genero">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE</th>

            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoGeneros->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_gen; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_gen; ?>
              </td>

              <td class="text-center">
                <a href="<?php echo site_url(); ?>/generos/editar/<?php echo $filaTemporal->id_gen; ?>" class="btn btn-warning"><i class="fa fa-pen"></i> Editar</a>
                <a href="<?php echo site_url() ?>/generos/procesarEliminacion/<?php echo $filaTemporal->id_gen; ?>"
                                             class="btn btn-danger">
                                              <i class="fa fa-trash"></i> Eliminar
                                            </a>
              </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3>No de encontraron generos</h3>
      </div>
    <?php endif; ?>
  </div>

</div>
</div>
