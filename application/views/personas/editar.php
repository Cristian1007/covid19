<div class="container">


<br><br>
<h1 class="text-center">EDITAR PERSONA</h1>
<br><br>
<form class="" action="<?php echo site_url(); ?>/personas/procesarActualizacion" method="post" enctype="multipart/form-data" id="frm_nuevo_personas">
  <div class="row">
    <div class="col-md-4">
      <input type="hidden" name="id_per" id="id_per" value="<?php echo $persona->id_per; ?>">
      <label for="">NOMBRE: </label><br>
    </div>
    <div class="col-md-7">
      <input class="form-control" type="text" name="nombre_per" id="nombre_per" value="<?php echo $persona->nombre_per; ?>" placeholder="Por favor ingrese la direccion"><br>

    </div>

  </div>
  <br>
  <div class="row">
  <div class="col-md-4">
    <label for="">APELLIDO: </label><br>

  </div>
  <div class="col-md-7">
    <input class="form-control" type="text" name="apellido_per" id="apellido_per" value="<?php echo $persona->apellido_per; ?>" placeholder="Por favor ingrese la direccion"><br>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
    <label for="">IDENTIFICACION: </label><br>

  </div>
  <div class="col-md-7">
    <input class="form-control" type="text" name="identificacion_per" id="identificacion_per" value="<?php echo $persona->identificacion_per; ?>" placeholder="Por favor ingrese la direccion"><br>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
  <label for="">ESTADO: </label><br>
  </div>
  <div class="col-md-7">
    <select class="form-control" name="estado_per"
    id="estado_per">
  <option value="">***SELECIONE UN ESTADO ***</option>
  <option value="1">ACTIVO</option>
  <option value="0">INACTIVO</option>
    </select>
  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
<label for="">DIRECCION: </label><br>
  </div>
  <div class="col-md-7">
    <input class="form-control" type="text" name="direccion_per" id="direccion_per" value="<?php echo $persona->direccion_per; ?>" placeholder="Por favor ingrese la direccion" required><br>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
    <label for="">FECHA DE NACIMIENTO: </label><br>

  </div>
  <div class="col-md-7">
    <input class="form-control" type="date" name="fecha_nacimiento_per" id="fecha_nacimiento_per" value="<?php echo $persona->fecha_nacimiento_per; ?>" placeholder="Por favor ingrese la direccion" required><br>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
    <label for="">EDAD: </label><br>

  </div>
  <div class="col-md-7">
    <input class="form-control" type="text" name="edad_per" id="edad_per" value="<?php echo $persona->edad_per; ?>" placeholder="Por favor ingrese la direccion" required><br>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
    <label for="">GENERO</label>

  </div>
  <div class="col-md-7">
    <select class="form-control" name="fk_id_gen"
    id="fk_id_gen_editar" value="<?php echo $genero->nombre_gen; ?> " required>

    <?php if ($listadoGeneros): ?>
    <option value="">***SELECIONE UN GENÉRO***</option>
    <?php foreach ($listadoGeneros->result() as $genero):  ?>
     <option value="<?php echo $genero->id_gen; ?>" >
       <?php echo $genero->nombre_gen; ?>
     </option>

    <?php endforeach; ?>
    <?php endif; ?>
    </select>
  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-7">
    <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
    &nbsp;&nbsp;&nbsp;

    <a href="<?php echo site_url(); ?>/personas/index" class="btn btn-warning">CANCELAR</a>
  </div>

</div>
<br>

</form>
</div>
<script type="text/javascript">

  $("#estado_per").val("<?php echo $persona->estado_per; ?>");
  $("#fk_id_gen_editar").val("<?php echo $persona->fk_id_gen; ?>");
  $("fecha_nacimiento_per").val("<?php echo $persona->fecha_nacimiento_per; ?>");
</script>
