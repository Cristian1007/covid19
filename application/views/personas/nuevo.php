<div class="container">


<br><br>
<h1 class="text-center">NUEVA PERSONA</h1>
<br><br>



<form class="" action="<?php echo site_url(); ?>/personas/guardarPersona" method="post" id="frm_nuevo_personas" enctype="multipart/form-data">

  <div class="row">
  <div class="col-md-4 text-right">
  <label for="">NOMBRE: </label><br>
</div>
  <div class="col-md-7">
  <input class="form-control" type="text" name="nombre_per" id="nombre_per" value="" placeholder="Por favor ingrese el nombre" required><br>
  </div></div>
  <div class="row">
  <div class="col-md-4 text-right">
  <label for="">APELLIDO: </label><br>
  </div>
  <div class="col-md-7">
  <input class="form-control" type="text" name="apellido_per" id="apellido_per" value="" placeholder="Por favor ingrese el apellido" required><br>
  </div>
  </div>
<div class="row">
  <div class="col-md-4 text-right">
  <label for="">IDENTIFICACION: </label><br>
  </div>
  <div class="col-md-7">
  <input class="form-control" type="text" name="identificacion_per" id="identificacion_per" value="" placeholder="Por favor ingrese la identificacion" required><br>
</div>
</div>
<div class="row">
  <div class="col-md-4 text-right">
  <label for="">ESTADO: </label><br>
  </div>
  <div class="col-md-7">
  <select class="form-control" name="estado_per"
  id="estado_per" required>
  </div>

  <div class="col-md-4 text-right">
<option value="">***SELECIONE UN ESTADO ***</option>
<option value="1">ACTIVO</option>
<option value="0">INACTIVO</option>
  </select>
    </div>
      </div>
      <div class="row">
        <div class="col-md-4 text-right">
<br>
  <label for="">DIRECCION: </label><br>
  </div>
  <br>
  <div class="col-md-7">
  <input class="form-control" type="text" name="direccion_per" id="direccion_per" value="" placeholder="Por favor ingrese la direccion" required><br>
  </div>
  </div>
  <div class="row">
    <div class="col-md-4 text-right">
  <label for="">FECHA DE NACIMIENTO: </label><br>
</div>
    <div class="col-md-7">
  <input class="form-control" type="date" name="fecha_nacimiento_per" id="fecha_nacimiento_per" value="" placeholder="Por favor ingrese la fecha" required><br>
</div>
</div>
<div class="row">
  <div class="col-md-4 text-right">
  <label for="">EDAD: </label><br>
  </div>
  <div class="col-md-7">
  <input class="form-control" type="text" name="edad_per" id="edad_per" value="" placeholder="Por favor ingrese la edad" required><br>
  </div>
  </div>
  <div class="row">
    <div class="col-md-4 text-right">
  <label for="">GENERO</label>
  </div>
<div class="col-md-7">
  <select class="form-control" name="fk_id_gen"
  id="fk_id_gen_editar" required>
<option value="">***SELECIONE UN GENÉRO***</option>
<?php if ($listadoGeneros): ?>

 <?php foreach ($listadoGeneros->result() as $genero):  ?>
   <option value="<?php echo $genero->id_gen; ?>" >
     <?php echo $genero->nombre_gen; ?>
   </option>

 <?php endforeach; ?>
<?php endif; ?>
  </select>

</div>
</div>
  <br>
  <br>
  <center>
  <div class="row">
  <div class="cold-md-4">
  </div>

<div class="cold-md-7">
  <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
  &nbsp;&nbsp;&nbsp;

  <a href="<?php echo site_url(); ?>/personas/index" class="btn btn-warning">CANCELAR</a>
    </div>
</form>

</div>
</center>
