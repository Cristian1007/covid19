<br><br><br>


<center>
<h1>LISTADO DE PERSONAS</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/personas/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="container">


<div class="row">

  <div class="col-md-12">
    <?php if ($listadoPersonas): ?>

      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE</th>
            <th class="text-center">APELLIDO</th>
            <th class="text-center">IDENTIFICACION</th>
            <th class="text-center">ESTADO</th>
            <th class="text-center">DIRECCION</th>
            <th class="text-center">FECHA DE NACIMIENTO</th>
            <th class="text-center">EDAD</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoPersonas->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->apellido_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->identificacion_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->estado_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->direccion_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->fecha_nacimiento_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->edad_per; ?>
              </td>
              <td class="text-center">
                <a href="<?php echo site_url(); ?>/personas/editar/<?php echo $filaTemporal->id_per; ?>" class="btn btn-warning"><i class="fa fa-pen"></i> Editar</a>


                  <a href="<?php echo site_url(); ?>/personas/procesarEliminacion/<?php echo $filaTemporal->id_per; ?>"

                                               class="btn btn-danger">
                                                <i class="fa fa-trash"> </i>ELIMINAR
                                              </a>


                 </td>
                     </tr>
                     <?php endforeach; ?>
                     </tbody>
                 </table>
</div>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3>No de encontraron personas</h3>
      </div>
    <?php endif; ?>
  </div>

</div>
