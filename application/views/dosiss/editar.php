<div class="container">


<br><br>
<h1 class="text-center">EDITAR DOSIS</h1>
<br><br>

<form class="" action="<?php echo site_url(); ?>/dosiss/procesarActualizacion" method="post" id="frm_nuevo_dosis" enctype="multipart/form-data">
  <br>
  <div class="row">
    <div class="col-md-4">
      <input type="hidden" name="id_dos" id="id_dos" value="<?php echo $dosis->id_dos; ?>"required>
      <label for=""> VACUNA</label>

    </div>
    <div class="col-md-7">
      <select class="form-control" name="fk_id_vac"
      id="fk_id_vac_editar" required>
    <option value="">***SELECIONE LA VACUNA***</option>
    <?php if ($listadoVacunas): ?>

     <?php foreach ($listadoVacunas->result() as $vacuna):  ?>
       <option value="<?php echo $vacuna->id_vac; ?>" >
         <?php echo $vacuna->nombre_vac; ?>
       </option>

     <?php endforeach; ?>
    <?php endif; ?>
      </select>
    </div>

  </div>
  <br>
  <div class="row">
    <div class="col-md-4">
<label for=""> PACIENTE</label>
    </div>
    <div class="col-md-7">
      <select class="form-control" name="fk_id_per"
      id="fk_id_per_editar" required>
    <option value="">***SELECIONE EL PACIENTE***</option>
    <?php if ($listadoPersonas): ?>

     <?php foreach ($listadoPersonas->result() as $persona):  ?>
       <option value="<?php echo $persona->id_per; ?>" >
         <?php echo $persona->nombre_per; ?> <?php echo $persona->apellido_per; ?>
       </option>

     <?php endforeach; ?>
    <?php endif; ?>
      </select>
    </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <label for=""> VACUNADOR</label>

      </div>
      <div class="col-md-7">
        <select class="form-control" name="fk_id_vacu"
        id="fk_id_vacu_editar" required>
      <option value="">***SELECIONE EL VACUNADOR***</option>
      <?php if ($listadoVacunador): ?>

       <?php foreach ($listadoVacunador->result() as $vacunador):  ?>
         <option value="<?php echo $vacunador->id_vacu; ?>" >
           <?php echo $vacunador->nombre_vacu; ?> <?php echo $vacunador->apellido_vacu; ?>
         </option>

       <?php endforeach; ?>
      <?php endif; ?>
        </select>
      </div>
      </div>
      <br>

  <div class="row">
    <div class="col-md-4">
<label for="">LUGAR: </label><br>
    </div>
    <div class="col-md-7">
      <input class="form-control" type="text" name="lugar_dos" id="lugar_dos" value="<?php echo $dosis->lugar_dos; ?>" placeholder="Por favor ingrese el lugar"required><br>

    </div>

  </div>
  <div class="row">
  <div class="col-md-4">
<label for="">FECHA: </label><br>
  </div>
  <div class="col-md-7">

    <input class="form-control" type="date" name="fecha_dos" id="fecha_dos" value="<?php echo $dosis->fecha_dos; ?>" placeholder="Por favor ingrese la fecha"><br>

  </div>

</div>
<div class="row">
  <div class="col-md-4">
<label for="">NUMERO DE LOTE: </label><br>
  </div>
  <div class="col-md-7">

    <input class="form-control" type="text" name="numero_lote_dos" id="numero_lote_dos" value="<?php echo $dosis->numero_lote_dos; ?>" placeholder="Por favor ingrese el numero de lote" required><br>

  </div>

</div>
<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-7">
    <br>
      <br>
      <button type="submit" name="button" class="btn btn-primary">ACTUALIZAR</button>
      &nbsp;&nbsp;&nbsp;

      <a href="<?php echo site_url(); ?>/dosiss/index" class="btn btn-warning">CANCELAR</a>
  </div>

</div>
</div>

</form>



  <script type="text/javascript">
   $("#fk_id_vac_editar").val("<?php echo $dosis->fk_id_vac; ?>");
   $("#fk_id_per_editar").val("<?php echo $dosis->fk_id_per; ?>");
   $("#fk_id_vacu_editar").val("<?php echo $dosis->fk_id_vacu; ?>");

  </script>
