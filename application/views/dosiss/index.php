<br><br><br>
<center>
<h1>LISTADO DE DOSIS</h1>
<hr>
<br>
<center>

<a class="btn btn-primary" href="<?php echo site_url(); ?>/dosiss/nuevo">Agregar nuevo</a>
<br><br><br>
<div class="row">
  <div class="col-md-1">

  </div>
  <div class="col-md-10">
    <?php if ($listadoDosiss): ?>

      <table class="table table-bordered table-hover table-striped" id="tbl_dosiss">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th class="text-center">PACIENTE</th>
            <th class="text-center">VACUNA</th>
            <th class="text-center">VACUNADOR</th>
            <th class="text-center">FECHA</th>
            <th class="text-center">LUGAR</th>
            <th class="text-center">NUMERO DE LOTE</th>
            <th class="text-center">OPCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($listadoDosiss->result() as $filaTemporal): ?>
            <tr>
              <td class="text-center">
                <?php echo $filaTemporal->id_dos; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_per; ?> <?php echo $filaTemporal->apellido_per; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_vac; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->nombre_vacu; ?> <?php echo $filaTemporal->apellido_vacu; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->fecha_dos; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->lugar_dos; ?>
              </td>
              <td class="text-center">
                <?php echo $filaTemporal->numero_lote_dos; ?>
              </td>

              <td class="text-center">
                <a href="<?php echo site_url(); ?>/dosiss/editar/<?php echo $filaTemporal->id_dos; ?>" class="btn btn-warning"><i class="fa fa-pen"></i> Editar</a>
                <a href="<?php echo site_url(); ?>/dosiss/procesarEliminacion/<?php echo $filaTemporal->id_dos; ?>"
                 class="btn btn-danger">
                  <i class="fa fa-trash"></i> Eliminar
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    <?php else: ?>
      <div class="alert alert-danger">
        <h3>No de encontraron Dosiss</h3>
      </div>
    <?php endif; ?>
  </div>
  <div class="col-md-1">

  </div>
</div>
<script type="text/javascript">
  $("#tbl_dosiss").DataTable({
    responsive: true,
 autoWidth: false,
 posistion: 'center',
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
    language: {
              url: "https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-MX.json"
          }
  });
</script>

<script type="text/javascript">
    function confirmarEliminacion(id_dos){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar la dosis de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/dosiss/procesarEliminacion/"+id_dos;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
