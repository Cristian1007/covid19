

<div class="container">

<hr>
<br>
<center>

    <h1>LISTADO DE USUARIOS</h1>
<br>
    <a href="<?php echo site_url(); ?>/usuarios/nuevo" class="btn btn-primary">
      <i class="fa fa-plus-circle"></i>
      Agregar Nuevo
    </a>
</center>

<br>
<br>
<div class="row">

  <div class="col-md-12">
<?php if ($listadoUsuarios): ?>
  <table class="table" id="tbl-usuarios">
        <thead>
            <tr>
              <th class="text-center">ID</th>

              <th class="text-center">APELLIDO</th>
              <th class="text-center">NOMBRE</th>
              <th class="text-center">CORREO</th>
              <th class="text-center">PERFIL</th>
              <th class="text-center">ESTADO</th>

              <th class="text-center">OPCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoUsuarios->result()
            as $filaTemporal): ?>
                  <tr>
                    <td class="text-center">
                      <?php echo $filaTemporal->id_usu; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->apellido_usu; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->nombre_usu; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->email_usu; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $filaTemporal->perfil_usu; ?>
                    </td>

                    <td class="text-center">

                        <div class="text-center" >
                            <?php echo $filaTemporal->estado_usu; ?>
                        </div>



                    </td>

                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/usuarios/editar/<?php echo $filaTemporal->id_usu; ?>" class="btn btn-warning">
                          <i class="fa fa-pen"></i> Editar
                        </a>
                          <a href="<?php echo site_url(); ?>/usuarios/procesarEliminacion/<?php echo $filaTemporal->id_usu; ?>"
                           class="btn btn-danger">
                            <i class="fa fa-trash"></i> Eliminar
                          </a>

                    </td>
                  </tr>
            <?php endforeach; ?>
        </tbody>
  </table>
<?php else: ?>
<div class="alert alert-danger">
    <h3>No se encontraron clientes registrados</h3>
</div>
<?php endif; ?>
</div>

</div>

</div>





<!--  -->
