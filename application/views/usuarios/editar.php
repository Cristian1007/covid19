<div class="container">


<br><br>
<h1 class="text-center">EDITAR USUARIO</h1>
<br><br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/usuarios/procesarActualizacion"
  method="post">

  <div class="row">
    <div class="col-md-4">
      <input type="hidden" name="id_usu" id="id_usu" value="<?php echo $usuario->id_usu; ?>" required >

        <label for="">APELLIDO</label>

    </div>
    <div class="col-md-7">
      <input class="form-control"
      value="<?php echo $usuario->apellido_usu; ?>"
      type="text" name="apellido_usu"
      id="apellido_usu"
      placeholder="Por favor Ingrese el apellido" required>
    </div>

  </div>
  <br>
  <div class="row">
  <div class="col-md-4">
  <label for="">NOMBRE</label>
  </div>
  <div class="col-md-7">
    <input class="form-control" value="<?php echo $usuario->nombre_usu; ?>"   type="text" name="nombre_usu" id="nombre_usu" placeholder="Por favor Ingrese el nombre" required>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
    <label for="">CORREO ELECTRÓNICO</label>

  </div>
  <div class="col-md-7">
    <input class="form-control" value="<?php echo $usuario->email_usu; ?>"    type="email" name="email_usu" id="email_usu" placeholder="Por favor Ingrese el correo" required>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
    <label for="">PASSWORD</label>

  </div>
  <div class="col-md-7">
    <input class="form-control"  value="<?php echo $usuario->password_usu; ?>" type="password" name="password_usu" id="password_usu" placeholder="Por favor Ingrese su contraseña" required>

  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
  <label for="">PERFIL</label>
  </div>
  <div class="col-md-7">
    <select class="form-control" name="perfil_usu" id="perfil_usu" required>
        <option value="">--Seleccione--</option>
        <option value="ADMINISTRADOR">ADMINISTRADOR</option>
        <option value="MEDICO">MEDICO</option>
    </select>
  </div>
  </div>
  <br>

<div class="row">
  <div class="col-md-4">
  <label for="">ESTADO</label>
  </div>
  <div class="col-md-7">
    <select class="form-control" name="estado_usu" id="estado_usu" required>
        <option value="">--Seleccione--</option>
        <option value=1>ACTIVO</option>
        <option value=0>INACTIVO</option>
    </select>
  </div>

</div>
<br>

</div>
<br>
<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-7">


  <button type="submit" name="button" class="btn btn-primary">
    GUARDAR
  </button>
  &nbsp;&nbsp;&nbsp;
  <a href="<?php echo site_url(); ?>/usuarios/index"
    class="btn btn-warning">
    <i class="fa fa-times"> </i> CANCELAR
  </a>
</div>
</div>
<br>

</form>
</div>
</div>
</div>

<script type="text/javascript">

   $("#estado_usu")
   .val("<?php echo $usuario->estado_usu; ?>");
   $("#perfil_usu")
   .val("<?php echo $usuario->perfil_usu; ?>");
</script>
</div>
