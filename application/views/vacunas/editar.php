<div class="container">

<br>
<h1 class="text-center">Actulizar Vacunas</h1>
<br>
<form class=""
action="<?php echo site_url('vacunas/procesarActualizacion'); ?>"
method="post" enctype="multipart/form-data"
id="frm_actualizar_usuario">

    <input type="hidden" name="id_vac" id="id_vac"
    value="<?php echo $vacuna->id_vac; ?>">
    <div class="row">
      <div class="col-md-4">
        <label for="">NOMBRE:</label><br>

      </div>
      <div class="col-md-7">
        <input type="text" name="nombre_vac"
        value="<?php echo $vacuna->nombre_vac; ?>"
        id="nombre_vac_editar" class="form-control" required> <br>



      </div>

    </div>
    <div class="row">
  <div class="col-md-4">
      <label for="">FABRICANTE:</label><br>
  </div>
  <div class="col-md-7">

    <input type="text" name="fabricante_vac"
    id="fabricante_vac_editar" class="form-control" value="<?php echo $vacuna->fabricante_vac; ?>" required> <br>


  </div>

</div>
<div class="row">
  <div class="col-md-4">
      <label for="">CANTIDAD:</label><br>
  </div>
  <div class="col-md-7">

      <input type="text" name="cantidad_vac"
      value="<?php echo $vacuna->cantidad_vac; ?>"
      id="cantidad_vac_editar" class="form-control" required> <br>

  </div>

</div>
<div class="row">
  <div class="col-md-4">
<label for="">ESTADO:</label>
  </div>
  <div class="col-md-7">
    <select class="form-control" name="estado_vac"
    id="estado_vac_editar" required>
     <option value="">Seleccione una opción</option>
     <option value="1">ACTIVO</option>
     <option value="0">INACTIVO</option>
    </select>
  </div>

</div>
<div class="row">
  <div class="col-md-4">

  </div>
  <div class="col-md-7">
    <br>
    <button type="submit" name="button"
    class="btn btn-success">
      <i class="fa fa-pen"></i> Actualizar
    </button>
    <a href="<?php echo site_url('vacunas/index')?>" class="btn btn-danger">Cancelar</a>
  </div>

</div>

</center>

</form>
<script type="text/javascript">
    $("#estado_vac_editar").val("<?php echo $vacuna->estado_vac; ?>");
</script>


</div>
