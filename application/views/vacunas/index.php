<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<br>
<h1 class="text-center">GESTION DE VACUNAS</h1>
<center>

   <!-- Trigger the modal with a button -->
   <a href="<?php echo site_url('vacunas/nuevo'); ?>" class="btn btn-success ">
     <i class="fa fa-plus-circle"></i>  Agregar Vacuna </a>

 </center>

  <?php if ($listadoVacunas): ?>

    <table class="table table-bordered table-hover" id="tbl_vacunas">
        <thead>
           <tr>
             <th class="text-center">ID</th>
             <th class="text-center">NOMBRE</th>
             <th class="text-center">FABRICANTE</th>
             <th class="text-center">CANTIDAD</th>
             <th class="text-center">ESTADO</th>
             <th class="text-center">ACCIONES</th>
           </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoVacunas->result() as $usuario): ?>
                <tr>
                    <td class="text-center">
                      <?php echo $usuario->id_vac; ?>
                    </td>
                    <br>
                    <center>

                    <td class="text-center">
                      <?php echo $usuario->nombre_vac; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $usuario->fabricante_vac; ?>
                    </td>
                    <td class="text-center">
                      <?php echo $usuario->cantidad_vac; ?>
                    </td>
                    <td class="text-center">
                      <?php if ($usuario->estado_vac=="1"): ?>
                        <div class="text-center" >
                          <p>ACTIVO</p>
                        </div>
                      <?php else: ?>
                        <div class="text-center">
                          <p>INACTIVO</p>
                        </div>
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                      <a href="<?php echo site_url() ?>/vacunas/editar/<?php echo $usuario->id_vac; ?>"
                       class="btn btn-warning">
                        <i class="fa fa-edit"></i> Editar
                      </a>
                      &nbsp;
                      <a href="<?php echo site_url() ?>/vacunas/procesarEliminacion/<?php echo $usuario->id_vac; ?>" class="btn btn-danger">
                        <i class="fa fa-trash">  </i> Eliminar
                      </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
  <?php else: ?>
      <br>
      <div class="alert alert-danger">
          No se encontraron Usuarios Registrados
      </div>
  <?php endif; ?>
  <script type="text/javascript">
