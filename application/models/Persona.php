<?php
  class Persona extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    public function insertar($datos){
      return $this->db->insert('persona',$datos);
    }

    public function consultarTodos(){
      $this->db->join('genero','genero.id_gen=persona.fk_id_gen');
      $listadoPersonas=$this->db->get('persona');
      if ($listadoPersonas->num_rows()>0) {
        return $listadoPersonas;
      }else{
        return false;
      }
    }

    public function eliminar($id_per){
      $this->db->where("id_per",$id_per);
      return $this->db->delete("persona");
    }

    public function actualizar($id_per,$datos){
      $this->db->where('id_per',$id_per);
      return $this->db->update('persona',$datos);
    }

    public function consultarPorId($id_per){
      $this->db->join('genero','genero.id_gen=persona.fk_id_gen');
      $this->db->where('id_per',$id_per);
      $persona=$this->db->get('persona');
      if ($persona->num_rows()>0) {
        return $persona->row();
      }else{
        return false;
      }
    }



  }
 ?>
