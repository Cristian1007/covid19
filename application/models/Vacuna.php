<?php
  class Vacuna extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    public function insertarVac($datos){
      return $this->db->insert('vacuna',$datos);
    }
    public function obtenerTodos(){
            //$this->db->order_by("apellido_usu","asc");
            $listado=$this->db->get("vacuna");
            if($listado->num_rows()>0){
              return $listado;
            }else{
              return false;
            }
          }

    public function consultarTodos(){
      $listadoVacunas=$this->db->get('vacuna');
      if ($listadoVacunas->num_rows()>0) {
        return $listadoVacunas;
      }else{
        return false;
      }
    }

    public function eliminar($id_vac){
      $this->db->where("id_vac",$id_vac);
      return $this->db->delete("vacuna");
    }
    public function obtenerPorId($id_vac){
          $this->db->where("id_vac",$id_vac);
          $vacuna=$this->db->get("vacuna");
          if($vacuna->num_rows()>0){
            return $vacuna->row();
          }else{
            return false;
          }
        }
    public function actualizar($id_vac, $data){
      $this->db->where('id_vac',$id_vac);
      return $this->db->update('vacuna',$data);
    }


    public function consultarPorId($id_vac){
      $this->db->where('id_vac',$id_vac);
      $vacuna=$this->db->get('vacuna');
      if ($vacuna->num_rows()>0) {
        return $vacuna->row();
      }else{
        return false;
      }
    }
    public function capturarPath($id_vac){
      $this->db->select("path_vac");
      $this->db->where("id_vac", $id_vac);
      $this->db->from("vacuna");

      $resultado = $this->db->get();

      return $resultado->row();

    }



  }
 ?>
