<?php
  class Vacunador extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    public function insertar($datos){
      return $this->db->insert('vacunador',$datos);
    }

    public function consultarTodos(){
      $listadoVacunadores=$this->db->get('vacunador');
      if ($listadoVacunadores->num_rows()>0) {
        return $listadoVacunadores;
      }else{
        return false;
      }
    }

    public function eliminar($id_vacu){
      $this->db->where("id_vacu",$id_vacu);
      return $this->db->delete("vacunador");
    }

    public function actualizar($id_vacu,$datos){
      $this->db->where('id_vacu',$id_vacu);
      return $this->db->update('vacunador',$datos);
    }

    public function consultarPorId($id_vacu){
      $this->db->where('id_vacu',$id_vacu);
      $vacunador=$this->db->get('vacunador');
      if ($vacunador->num_rows()>0) {
        return $vacunador->row();
      }else{
        return false;
      }
    }



  }
 ?>
